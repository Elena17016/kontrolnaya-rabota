﻿using WpfApp17.Model;

namespace WpfApp17.Helper
{
    public class FindPublish
    {
        int id;

        public FindPublish(int id)
        {
            this.id = id;
        }

        public bool PublishPredicate(Publish publish)
        {
            return publish.Id == id;
        }
    }
}
