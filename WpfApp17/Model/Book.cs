﻿using System;
using WpfApp17.ViewModel;

namespace WpfApp17.Model
{
    public class Book
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int PublishId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string YearPublish { get; set; }
        public int CountPage { get; set; }
        public bool Hardcover { get; set; }
        public string Abstract { get; set; }
        public bool Status { get; set; }

        public Book() { }

        public Book(int id, int authorId, int publishId, string title, string code, 
            string yearPublish, int countPage, bool hardcover, string _abstract, bool status)
        {
            this.Id = id;
            this.AuthorId = authorId;
            this.PublishId = publishId;
            this.Title = title;
            this.Code = code;
            this.YearPublish = yearPublish;
            this.CountPage = countPage;
            this.Hardcover = hardcover;
            this.Abstract = _abstract;
            this.Status = status;
        }

        public Book CopyFromBookDPO(BookDPO b)
        {
            AuthorViewModel vmAuthor = new AuthorViewModel();
            int authorId = 0;
            foreach(var a in vmAuthor.ListAuthor)
            {
                if(a.LastName == b.Author)
                {
                    authorId = a.Id;
                    break;
                }
            }

            PublishViewModel vmPublish = new PublishViewModel();
            int publishId = 0;
            foreach(var p in vmPublish.ListPublish)
            {
                if(p.NamePublisher == b.Publish)
                {
                    publishId = p.Id;
                    break;
                }
            }

            if(authorId != 0 & publishId != 0)
            {
                this.Id = b.Id;
                this.AuthorId = authorId;
                this.PublishId = publishId;
                this.Title = b.Title;
                this.Code = b.Code;
                this.YearPublish = b.YearPublish;
                this.CountPage = b.CountPage;
                this.Hardcover = b.Hardcover;
                this.Abstract = b.Abstract;
                this.Status = b.Status;
            }

            return this;
        }
    }
}
