﻿using System;
using System.Collections.Generic;
using WpfApp17.ViewModel;

namespace WpfApp17.Model
{
    public class BookDPO
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Publish { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string YearPublish { get; set; }
        public int CountPage { get; set; }
        public bool Hardcover { get; set; }
        public string Abstract { get; set; }
        public bool Status { get; set; }

        public BookDPO() { }

        public BookDPO(int id, string author, string publish, string title, string code, 
            string yearPublish, int countPage, bool hardcover, string _abstract, bool status)
        {
            this.Id = id;
            this.Author = author;
            this.Publish = publish;
            this.Title = title;
            this.Code = code;
            this.YearPublish = yearPublish;
            this.CountPage = countPage;
            this.Hardcover = hardcover;
            this.Abstract = _abstract;
            this.Status = status;
        }

        public BookDPO CopyFromBook(Book book)
        {
            BookDPO bookDpo = new BookDPO();
            AuthorViewModel vmAuthor = new AuthorViewModel();
            string author = string.Empty;
            foreach(var a in vmAuthor.ListAuthor)
            {
                if(a.Id == book.AuthorId)
                {
                    author = a.LastName;
                    break;
                }
            }

            PublishViewModel vmPublish = new PublishViewModel();
            string publish = string.Empty;
            foreach(var p in vmPublish.ListPublish)
            {
                if(p.Id == book.PublishId)
                {
                    publish = p.NamePublisher;
                    break;
                }
            }

            if(author != string.Empty & publish != string.Empty)
            {
                bookDpo.Id = book.Id;
                bookDpo.Author = author;
                bookDpo.Publish = publish;
                bookDpo.Title = book.Title;
                bookDpo.Code = book.Code;
                bookDpo.YearPublish = book.YearPublish;
                bookDpo.CountPage = book.CountPage;
                bookDpo.Hardcover = book.Hardcover;
                bookDpo.Abstract = book.Abstract;
                bookDpo.Status = book.Status;

            }
            return bookDpo;
        }

        public BookDPO ShallowCopy()
        {
            return (BookDPO)this.MemberwiseClone();
        }
    }
}
