﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using WpfApp17.ViewModel;
using WpfApp17.Helper;
using WpfApp17.Model;
using System.Linq;

namespace WpfApp17.View
{
    /// <summary>
    /// Логика взаимодействия для WindowBook.xaml
    /// </summary>
    public partial class WindowBook : Window
    {
        private BookViewModel vmBook;
        private AuthorViewModel vmAuthor;
        private PublishViewModel vmPublish;

        private List<Author> authors;
        private List<Publish> publishes;

        private ObservableCollection<BookDPO> bookDPO;

        public WindowBook()
        {
            InitializeComponent();

            vmBook = new BookViewModel();

            vmAuthor = new AuthorViewModel();
            authors = vmAuthor.ListAuthor.ToList();

            vmPublish = new PublishViewModel();
            publishes = vmPublish.ListPublish.ToList();

            bookDPO = new ObservableCollection<BookDPO>();
            foreach(var book in vmBook.ListBook)
            {
                BookDPO b = new BookDPO();
                b = b.CopyFromBook(book);
                bookDPO.Add(b);
            }
            lvBook.ItemsSource = bookDPO;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            WindowNewBook wnBook = new WindowNewBook
            {
                Title = "Новая книга",
                Owner = this
            };
            int maxIdBook = vmBook.MaxId() + 1;
            BookDPO book = new BookDPO
            {
                Id = maxIdBook,
                YearPublish = "25.10.2020"
            };
            wnBook.DataContext = book;
            wnBook.CbAuthor.ItemsSource = authors;
            wnBook.CbPublish.ItemsSource = publishes;
            if(wnBook.ShowDialog() == true)
            {
                Author a = (Author)wnBook.CbAuthor.SelectedValue;
                book.Author = a.LastName;
                Publish p = (Publish)wnBook.CbPublish.SelectedValue;
                book.Publish = p.NamePublisher;
                book.Hardcover = (bool)wnBook.chbHardcover.IsChecked;
                book.Status = (bool)wnBook.ChbStatus.IsChecked;
                
                bookDPO.Add(book);

                Book b = new Book();
                b = b.CopyFromBookDPO(book);
                vmBook.ListBook.Add(b);
                vmBook.SaveChanges(vmBook.ListBook);
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            WindowNewBook wnBook = new WindowNewBook
            {
                Title = "Редактирование данных",
                Owner = this
            };
            BookDPO bDPO = (BookDPO)lvBook.SelectedValue;
            BookDPO tempBookDPO;
            if (bDPO != null)
            {
                tempBookDPO = bDPO.ShallowCopy();

                wnBook.DataContext = tempBookDPO;
                wnBook.CbAuthor.ItemsSource = authors;
                wnBook.CbAuthor.Text = tempBookDPO.Author;

                wnBook.CbPublish.ItemsSource = publishes;
                wnBook.CbPublish.Text = tempBookDPO.Publish;

                wnBook.chbHardcover.IsChecked = tempBookDPO.Hardcover;
                wnBook.ChbStatus.IsChecked = tempBookDPO.Status;
                if (wnBook.ShowDialog() == true)
                {
                    Author a = (Author)wnBook.CbAuthor.SelectedValue;
                    bDPO.Author = a.LastName;
                    Publish p = (Publish)wnBook.CbPublish.SelectedValue;
                    bDPO.Publish = p.NamePublisher;
                    bDPO.Title = tempBookDPO.Title;
                    bDPO.Code = tempBookDPO.Code;
                    bDPO.YearPublish = tempBookDPO.YearPublish;
                    bDPO.CountPage = tempBookDPO.CountPage;
                    bDPO.Hardcover = (bool)wnBook.chbHardcover.IsChecked;
                    bDPO.Abstract = tempBookDPO.Abstract;
                    bDPO.Status = (bool)wnBook.ChbStatus.IsChecked;

                    lvBook.ItemsSource = null;
                    lvBook.ItemsSource = bookDPO;

                    FindBook finder = new FindBook(bDPO.Id);
                    List<Book> listBook = vmBook.ListBook.ToList();
                    Book b = listBook.Find(new Predicate<Book>(finder.BookPredicate));
                    b = b.CopyFromBookDPO(bDPO);

                    vmBook.SaveChanges(vmBook.ListBook);
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать книгу для редактированния", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            BookDPO book = (BookDPO)lvBook.SelectedItem;
            if(book != null)
            {
                MessageBoxResult result = MessageBox.Show("Удалить данные по книге: \n" + book.Title, 
                    "Предупреждение", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if(result == MessageBoxResult.OK)
                {
                    bookDPO.Remove(book);

                    Book b = new Book();
                    b = b.CopyFromBookDPO(book);
                    vmBook.ListBook.Remove(b);

                    vmBook.SaveChanges(vmBook.ListBook);
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать данные по книге для удаления", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
