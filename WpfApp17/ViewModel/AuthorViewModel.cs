﻿using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json;

using WpfApp17.Model;

namespace WpfApp17.ViewModel
{
    public class AuthorViewModel
    {
        readonly string path = @"C:\Users\Di0nisBloody\source\repos\WpfApp17\WpfApp17\DataModels\AuthorData.json";
        string _jsonAuthor = string.Empty;
        public string Error { get; set; }

        public ObservableCollection<Author> ListAuthor { get; set; } = new ObservableCollection<Author>();

        public AuthorViewModel()
        {
            ListAuthor = LoadAuthor();
        }

        public ObservableCollection<Author> LoadAuthor()
        {
            _jsonAuthor = File.ReadAllText(path);
            if(_jsonAuthor != null)
            {
                ListAuthor = JsonConvert.DeserializeObject<ObservableCollection<Author>>(_jsonAuthor);
                return ListAuthor;
            }
            else
            {
                return null;
            }
        }

        public void SaveChanges(ObservableCollection<Author> listAuthor)
        {
            var jsonAuthor = JsonConvert.SerializeObject(listAuthor);
            try
            {
                using (StreamWriter writer = File.CreateText(path))
                {
                    writer.Write(jsonAuthor);
                }
            }
            catch(IOException e)
            {
                Error = "Ошибка записи json файла /n" + e.Message;
            }
        }

        public int MaxId()
        {
            int max = 0;
            foreach(var a in this.ListAuthor)
            {
                if(max < a.Id)
                {
                    max = a.Id;
                }
            }
            return max;
        }
    }
}
