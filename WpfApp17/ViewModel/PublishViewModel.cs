﻿using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json;

using WpfApp17.Model;

namespace WpfApp17.ViewModel
{
    public class PublishViewModel
    {
        readonly string path = @"C:\Users\Di0nisBloody\source\repos\WpfApp17\WpfApp17\DataModels\PublishData.json";
        string _jsonPublish = string.Empty;
        public string Error { get; set; }

        public ObservableCollection<Publish> ListPublish { get; set; } = new ObservableCollection<Publish>();

        public PublishViewModel()
        {
            ListPublish = LoadPublish();
        }

        public ObservableCollection<Publish> LoadPublish()
        {
            _jsonPublish = File.ReadAllText(path);
            if (_jsonPublish != null)
            {
                ListPublish = JsonConvert.DeserializeObject<ObservableCollection<Publish>>(_jsonPublish);
                return ListPublish;
            }
            else
            {
                return null;
            }
        }

        public void SaveChanges(ObservableCollection<Publish> listPublish)
        {
            var jsonPublish = JsonConvert.SerializeObject(listPublish);
            try
            {
                using (StreamWriter writer = File.CreateText(path))
                {
                    writer.Write(jsonPublish);
                }
            }
            catch (IOException e)
            {
                Error = "Ошибка записи json файла /n" + e.Message;
            }
        }

        public int MaxId()
        {
            int max = 0;
            foreach (var p in this.ListPublish)
            {
                if (max < p.Id)
                {
                    max = p.Id;
                }
            }
            return max;
        }
    }
}
