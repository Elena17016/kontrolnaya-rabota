﻿using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json;

using WpfApp17.Model;

namespace WpfApp17.ViewModel
{
    public class BookViewModel
    {
        readonly string path = @"C:\Users\Di0nisBloody\source\repos\WpfApp17\WpfApp17\DataModels\BookData.json";
        string _jsonBook = string.Empty;
        public string Error { get; set; }

        public ObservableCollection<Book> ListBook { get; set; } = new ObservableCollection<Book>();

        public BookViewModel()
        {
            ListBook = LoadBook();
        }

        public ObservableCollection<Book> LoadBook()
        {
            _jsonBook = File.ReadAllText(path);
            if (_jsonBook != null)
            {
                ListBook = JsonConvert.DeserializeObject<ObservableCollection<Book>>(_jsonBook);
                return ListBook;
            }
            else
            {
                return null;
            }
        }

        public void SaveChanges(ObservableCollection<Book> listBook)
        {
            var jsonBook = JsonConvert.SerializeObject(listBook);
            try
            {
                using (StreamWriter writer = File.CreateText(path))
                {
                    writer.Write(jsonBook);
                }
            }
            catch (IOException e)
            {
                Error = "Ошибка записи json файла /n" + e.Message;
            }
        }

        public int MaxId()
        {
            int max = 0;
            foreach (var b in this.ListBook)
            {
                if (max < b.Id)
                {
                    max = b.Id;
                }
            }
            return max;
        }
    }
}
